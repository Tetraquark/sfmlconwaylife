/*
 * 	Another implementation of the "Conway's Game of Life".
 *
 *	@author		Tetraquark	| tetraquark.ru
 * 	@change     22.08.2015	Tetraquark
 * 	@version	0.2
 */

#include <SFML/Graphics.hpp>
#include "App.hpp"

#define VERSION		"0.1"
#define CONFIG_FILE "config.json"
#define MAX_SPEED	32

int main(){
	App app(CONFIG_FILE);
	app.runApp();
	return 0;
}
