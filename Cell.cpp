#include "Cell.hpp"

Cell::Cell(){
	state = Cell::DEAD;
}

Cell::Cell(float __x, float __y, float __width, float __heigth, sf::Color __color){
	cellInit(__x, __y, __width, __heigth, __color, Cell::DEAD);
}

Cell::Cell(float __x, float __y, float __width, float __heigth, sf::Color __color, Cell::CellState __state){
	cellInit(__x, __y, __width, __heigth, __color, __state);
}

void Cell::cellInit(float __x, float __y, float __width, float __heigth, sf::Color __color, Cell::CellState __state){
	x = __x;
	y = __y;
	width = __width;
	height = __heigth;
	color = __color;
	updateFlag = false;

	cellShape.setPosition(x, y);
	cellShape.setSize(sf::Vector2f(width, height));
	cellShape.setFillColor(color);
	cellShape.setOutlineThickness(2.f);
	cellShape.setOutlineColor(sf::Color::Black);

	state = __state;
}

void Cell::draw(sf::RenderWindow *__window){
	cellShape.setFillColor(color);
	__window->draw(cellShape);
}

int Cell::posCompare(sf::Vector2i __position){
	if(__position.x > x && __position.x < x + width)
		if(__position.y > y && __position.y < y + height)
			return 1;
	return 0;
}

sf::Vector2f Cell::getPosition(){
	sf::Vector2f position(x, y);
	return position;
}

float Cell::getX(){
	return x;
}

float Cell::getY(){
	return y;
}

Cell::CellState Cell::getState(){
	return state;
}

bool Cell::getUpdateFlag(){
	return updateFlag;
}

void Cell::setPosition(sf::Vector2f __position){
	x = __position.x;
	y = __position.y;
}

void Cell::setX(float __x){
	x = __x;
}

void Cell::setY(float __y){
	y = __y;
}

void Cell::setState(Cell::CellState __state, sf::Color __color){
	state = __state;
	color = __color;
}

void Cell::setUpdateFlag(bool __flag){
	updateFlag = __flag;
}
