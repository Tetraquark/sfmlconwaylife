/*
 * 	Class description here.
 *
 *	@author		Tetraquark	| tetraquark.ru
 * 	@change     16.08.2015	Tetraquark
 */

#ifndef CONFIGLOADER_HPP_
#define CONFIGLOADER_HPP_

#include <fstream>
#include "json/json.h"

class ConfigLoader{

private:
	std::ifstream confFile;
	Json::Reader reader;
	Json::Value root;
	std::string confAsString;

public:
	Json::Value LoadConfig(char* fileName);
};


#endif /* CONFIGLOADER_HPP_ */
