# README

## What is it?

SfmlConwayLife - is another implementation of the "Conway's Game of Life" mathematical automaton which simulate primitive cellular life. 
This implementation is based on a simple and very inefficient algorithm that requires a big computer performance for big world size (more than 100x100).

Used SFML 2.3.1 library for graphics.

## Controls and Settings

All application settings placed in a configuration file *config.json*. 
You can load different world patterns from directory "*data/patterns*" and change setting "**pattern**" in configuration file. Each pattern has a minimal world size requirement, which is displayed in the pattern file name.

**Left arrow key** - reduce speed of the simulation.
**Right arrow key** - increase speed of the simulation.
**Space bar button** - start/stop the simulation.
**Left mouse button** - create a living cell.
**Right mouse button** - move the camera.

## ��� ���?

SfmlConwayLife - ��� ��������� ���������� ��������������� �������� ����� ������ ��� ����� ��������� ��������� "���� �����", � ������� ����������� ��������� �����, �������� ������� ������� �� ���������� ������� ��������.
������ ���������� ��������� �������� �������� ������ ������� � �����������, ��� ������ ���������� �� ���������� � �������������� ��������� ����������. ���������� �������� �������� �������� ��� ��� �������� ���� ����� ��� 100x100.
����� �������� ������� �����: [http://tetraquark.ru/archives/74](http://tetraquark.ru/archives/74).

## ��������� � ����������

��� �������� ��������� ���������� ��������� � ���������������� ����� *config.json*.
������� ����������� ��������� ��� ������� ������� ���������� ���� � ������������������ ������� ����� ������. ��� �������� ������� ���������� ������� ���� �� ����� ������� (��� ��������� � �������� "*data/patterns*") � ���������������� ����� � ���� "**pattern**". ����� - ������ ������ ����� ���������� � ����������� �������� ����, ��� ���������� � ����� ����� �������.

**������ ����� �������** - ��������� �������� ���������.
**������ ������ �������** - ��������� �������� ���������.
**������ �������** - �����/���� �������� ���������.
**����� ������ ����** - ������� ����� ������.
**������ ������ ����** - ����������� ������.

##### Other
Current version is 0.2.
Last update is 2015-08-26.

By Tetraquark | [tetraquark.ru](http://tetraquark.ru/).

![scl_screen.png](http://tetraquark.ru/wp-content/uploads/2015/08/scl_screen.png)

