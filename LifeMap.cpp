#include "LifeMap.hpp"
#include <iostream>
LifeMap::LifeMap(int __mapWidth, int __mapHeight, float __cellWidth, float __cellHeight, sf::Color __deadColor, sf::Color __lifeColor, sf::Color __unFoundCellColor, int __interfaceHeight){

	mapWidth = __mapWidth;
	mapHeight = __mapHeight;

	cellWidth = __cellWidth;
	cellHeight = __cellHeight;

	deadCellColor = __deadColor;
	lifeCellColor = __lifeColor;
	unFoundCellColor = __unFoundCellColor;
	borderCellColor = sf::Color::Cyan;

	interfaceHeight = __interfaceHeight;

	// create map
	for(int i = 0; i < mapWidth; i++){
		std::vector<Cell> temp;
		for(int j = 0; j < mapHeight; j++){
			if(i == 0 || i == mapWidth - 1 || j == 0 || j == mapHeight - 1)
				temp.push_back(Cell(i * cellWidth, j * cellHeight + interfaceHeight,
									cellWidth, cellHeight, borderCellColor, Cell::BORDER));
			else
				temp.push_back(Cell(i * cellWidth, j * cellHeight + interfaceHeight,
					cellWidth, cellHeight, unFoundCellColor));
		}
		currentCells.push_back(temp);
		futureCells = currentCells;
	}
}

void LifeMap::drawMap(sf::RenderWindow *window){
	for(int i = 0; i < currentCells.size(); i++)
		for(int j = 0; j < currentCells[i].size(); j++){

			//if(currentCells[i][j].getState() == Cell::LIFE)
				//std::cout << i << " " << j << std::endl;
			currentCells[i][j].draw(window);

		}
	//std::cout << "-----------------" << std::endl;
}

void LifeMap::updateMap(){
	futureCells = currentCells;

	std::vector<sf::Vector2i> updateCells;
	for(int i = 1; i < currentCells.size() - 1; i++){
		for(int j = 1; j < currentCells[i].size() - 1; j++){
			if(currentCells[i][j].getState() == Cell::LIFE){
				int cx = i - 1, cy = j - 1;
				for(int k = 0; k < 3; k++){
					for(int z = 0; z < 3; z++){
						if(!currentCells[cx + k][cy + z].getUpdateFlag()){
							updateCells.push_back(sf::Vector2i(cx + k, cy + z));
							currentCells[cx + k][cy + z].setUpdateFlag(true);
						}
					}
				}
			}
		}
	}

	for(int i = 0; i < updateCells.size(); i++){
			updateCell(updateCells[i].x, updateCells[i].y);
	}

	currentCells = futureCells;
}

// Vector2i.x == i
// Vector2i.y == j
void LifeMap::setLifeCells(std::vector<sf::Vector2i> __cells){
	for(int i = 0; i < __cells.size(); i++)
		if(currentCells[__cells[i].x][__cells[i].y].getState() != Cell::BORDER)
			currentCells[__cells[i].x][__cells[i].y].setState(Cell::LIFE, lifeCellColor);
}

void LifeMap::acceptLifeCells(){
	for(int i = 0; i < currentCells.size(); i++)
		for(int j = 0; j < currentCells.size(); j++)
			if(currentCells[i][j].getState() == Cell::BIRTH)
				currentCells[i][j].setState(Cell::LIFE, lifeCellColor);
}

void LifeMap::setBirthCell(sf::Vector2i __coords){
	for(int i = 0; i < currentCells.size(); i++)
		for(int j = 0; j < currentCells.size(); j++)
			if(currentCells[i][j].posCompare(__coords))
				if(currentCells[i][j].getState() != Cell::BIRTH && currentCells[i][j].getState() != Cell::BORDER)
					currentCells[i][j].setState(Cell::BIRTH, sf::Color::Red);
}

void LifeMap::setDeadCellColor(sf::Color __color){
	deadCellColor = __color;
}

void LifeMap::setLifeCellColor(sf::Color __color){
	lifeCellColor = __color;
}

//private
int LifeMap::getCellSign(int number){
	if(number == 0)
		return -1;
	if(number == 1)
		return 0;

	return 1;
}

//private
void LifeMap::updateCell(int __i, int __j){

	int lifeCount = 0;
	int iSign = -1;
	int jSign = -1;

	//if(__i != mapWidth - 1 && __j != mapHeight - 1 && __i != 0 && __j != 0){
	if(currentCells[__i][__j].getState() != Cell::BORDER){
		for(int i = 0; i < 3; i++){
			iSign = getCellSign(i);
			for(int j = 0; j < 3; j++){
				jSign = getCellSign(j);
				if(!(i == 1 && j == 1))
					if(currentCells[__i + iSign][__j + jSign].getState() == Cell::LIFE)
						lifeCount++;
			}
		}
		if(currentCells[__i][__j].getState() == Cell::DEAD){
			if(lifeCount == 3){
				futureCells[__i][__j].setState(Cell::LIFE, lifeCellColor);
			}
		}
		if(currentCells[__i][__j].getState() == Cell::LIFE){
			if(lifeCount < 2 || lifeCount > 3)
				futureCells[__i][__j].setState(Cell::DEAD, deadCellColor);
		}
	}
	else{
		futureCells[__i][__j].setState(Cell::BORDER, borderCellColor);
	}
	futureCells[__i][__j].setUpdateFlag(false);
	currentCells[__i][__j].setUpdateFlag(false);
}
