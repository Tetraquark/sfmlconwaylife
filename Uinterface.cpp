#include "Uinterface.hpp"

Uinterface::Uinterface(int __charSize, sf::Color __charColor){
	charSize = __charSize;
	charColor = __charColor;

	loadFont(DEFAULT_FONT);

	speedPos.x = 5.f;
	speedPos.y = 5.f;
	speedText.setPosition(speedPos);
	speedText.setColor(charColor);
	speedText.setFont(charFont);
	speedText.setCharacterSize(charSize);

	stepsPos.x = speedPos.x + 150.f;
	stepsPos.y = 5.f;
	stepsText.setPosition(stepsPos);
	stepsText.setColor(charColor);
	stepsText.setFont(charFont);
	stepsText.setCharacterSize(charSize);

	statePos.x = stepsPos.x + 150.f;
	statePos.y = 5.f;
	gameState.setPosition(statePos);
	gameState.setColor(charColor);
	gameState.setFont(charFont);
	gameState.setCharacterSize(charSize);
}

int Uinterface::loadFont(char* __fontPath){
	fontPath = __fontPath;
	if (!charFont.loadFromFile(fontPath))
	    return 0;
	return 1;
}

void Uinterface::setSpeed(int __speed){
	speedStr = STR_SPEED + intToString(__speed);
	speedText.setString(speedStr);
}

void Uinterface::setStepsNum(long int __num){
	stepsStr = STR_STEPS + intToString(__num);
	stepsText.setString(stepsStr);
}

void Uinterface::drawUI(sf::RenderWindow *__window){
	sf::Vector2f uiPos = __window->mapPixelToCoords(sf::Vector2i(stepsPos.x, stepsPos.y));
	speedText.setPosition(uiPos.x - 150.f, uiPos.y);
	__window->draw(speedText);
	stepsText.setPosition(uiPos.x + 150.f, uiPos.y);
	__window->draw(stepsText);
	gameState.setPosition(uiPos.x + 300.f, uiPos.y);
	__window->draw(gameState);
}

void Uinterface::setState(std::string __state){
	gameState.setString(STR_STATE + __state);
}

std::string Uinterface::intToString(float __value){
	std::ostringstream convert;
	convert << __value;
	return convert.str();
}
