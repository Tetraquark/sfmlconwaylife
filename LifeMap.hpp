/*
 * 	Class description here.
 *
 *	@author		Tetraquark	| tetraquark.ru
 * 	@change     22.08.2015	Tetraquark
 */

#ifndef LIFEMAP_HPP_
#define LIFEMAP_HPP_

#include <vector>
#include <iostream>
#include "Cell.hpp"

class LifeMap{

private:
	int interfaceHeight;

	std::vector<std::vector<Cell> > currentCells;
	std::vector<std::vector<Cell> > futureCells;

	int mapWidth;
	int mapHeight;

	float cellWidth, cellHeight;
	sf::Color deadCellColor, lifeCellColor, borderCellColor, unFoundCellColor;

	void updateCell(int __i, int __j);
	int getCellSign(int number);
public:
	LifeMap(int __mapWidth, int __mapHeight, float __cellWidth, float __cellHeight, sf::Color __deadColor, sf::Color __lifeColor, sf::Color __unFoundCellColor, int __interfaceHeight);

	void drawMap(sf::RenderWindow *window);
	void updateMap();

	void setDeadCellColor(sf::Color __color);
	void setLifeCellColor(sf::Color __color);

	void setLifeCells(std::vector<sf::Vector2i> __cells);	//not setter
	void acceptLifeCells();
	void setBirthCell(sf::Vector2i __coords);
};


#endif /* LIFEMAP_HPP_ */
