#include "App.hpp"

App::App(){
	initApp(NULL);
}

App::App(char* __configFilePath){
	initApp(__configFilePath);
}

void App::initApp(char* __configFilePath){
	FrameTime = sf::seconds(1.f / 60.f);
	steps = 0;
	updatingMap = 0;
	createLiveCells = 0;
	slideMap = false;

	configFile = __configFilePath;
	if(configFile != NULL)
		loadConfigure(__configFilePath);

	window = new sf::RenderWindow(sf::VideoMode(winWidth, winHeight), "SfmlConwayLife");
	view = new sf::View(sf::FloatRect(0, 0, winWidth, winHeight));
	window->setView(*view);

	sf::Color cl(0, 102, 255);
	map = new LifeMap(mapWidth, mapHeight, cellWidth, cellHeight, cl, sf::Color::Green, sf::Color::Blue, 40);
	appSpeed = 140;
	gui = new Uinterface(18, sf::Color::White);
	gui->setSpeed(appSpeed);
	gui->setStepsNum(steps);
	gui->setState("Pause");

	loadAndSetPattern(patternFile.c_str());
}

/*
 * Private method. Load configuration of application
 * from JSON file.
 */
void App::loadConfigure(char* __configFilePath){
	jsonConf = cfLoad.LoadConfig(__configFilePath);
	patternFile = jsonConf["pattern"].asString();
	winWidth = jsonConf["window"]["width"].asInt();
	winHeight = jsonConf["window"]["height"].asInt();
	mapWidth = jsonConf["map_cell_size"]["width"].asInt();
	mapHeight = jsonConf["map_cell_size"]["height"].asInt();
	cellWidth = jsonConf["cell"]["width"].asInt();
	cellHeight = jsonConf["cell"]["height"].asInt();
}

void App::runApp(){
	sf::Vector2i pastMousePos;
	sf::Vector2i currMousePos;
	sf::Clock clock;
	sf::Time prevTime = sf::Time::Zero;

	while(window->isOpen()){
		sf::Time dt = clock.restart();
		prevTime += dt;

		pastMousePos = currMousePos;
		currMousePos = sf::Mouse::getPosition();

		sf::Event event;
		gui->setStepsNum(steps);
		while(window->pollEvent(event)){
			if(event.type == sf::Event::Closed)
				window->close();

			if(event.type == sf::Event::KeyPressed){
				if(event.key.code == sf::Keyboard::Space){
					if(updatingMap){
						updatingMap = 0;
						gui->setState("Pause");
					}
					else{
						updatingMap = 1;
						gui->setState("Action");
					}
				}

				if(event.key.code == sf::Keyboard::Left){
					if(appSpeed > 0){
						FrameTime = sf::seconds(FrameTime.asSeconds() + FrameTime.asSeconds() / 10);
						appSpeed -= 5;
						gui->setSpeed(appSpeed);
					}
				}

				if(event.key.code == sf::Keyboard::Right){
					if(appSpeed < MAX_APP_SPEED){
						FrameTime = sf::seconds(FrameTime.asSeconds() - FrameTime.asSeconds() / 10);
						appSpeed += 5;
						gui->setSpeed(appSpeed);
					}
				}
			}

			if(event.type == sf::Event::MouseButtonPressed){
				if(event.mouseButton.button == sf::Mouse::Left){
					createLiveCells = 1;
				}

				if(event.mouseButton.button == sf::Mouse::Right){
					slideMap = true;
				}

			}

			if(event.type == sf::Event::MouseButtonReleased){
				if(event.mouseButton.button == sf::Mouse::Left){
					map->acceptLifeCells();
					createLiveCells = 0;
				}

				if(event.mouseButton.button == sf::Mouse::Right){
					slideMap = false;
				}
			}

			if(event.type == sf::Event::MouseWheelMoved){
				view->zoom(1.f + event.mouseWheel.delta * -0.1f);
				window->setView(*view);
			}

		}
		window->setView(*view);

		sf::Vector2f mouseDelta(pastMousePos.x - currMousePos.x, pastMousePos.y - currMousePos.y);
		if(slideMap){
			view->move(mouseDelta.x, mouseDelta.y);
		}

		while(prevTime > FrameTime){
			prevTime -= FrameTime;

			if(createLiveCells){
				sf::Vector2i mPos;
				mPos.x = window->mapPixelToCoords(sf::Mouse::getPosition(*window)).x;
				mPos.y = window->mapPixelToCoords(sf::Mouse::getPosition(*window)).y;
				map->setBirthCell(mPos);
			}

			if(updatingMap){
				steps++;
				map->updateMap();
			}

		}

		window->clear();
		map->drawMap(window);
		window->setView(window->getDefaultView());
		gui->drawUI(window);
		window->display();
	}
}

int App::loadAndSetPattern(const char* __fileName){
	std::ifstream patternFile;
	patternFile.open(__fileName);
	if(!patternFile.is_open())
		return 0;

	std::vector<sf::Vector2i> patternCells;
	std::string xBuff, yBuff;

	int maxX = 0, maxY = 0;

	while(!patternFile.eof()){
		patternFile >> xBuff;
		patternFile >> yBuff;
		int x = std::atoi(xBuff.c_str());
		if(x > maxX)
			maxX = x;
		int y = std::atoi(yBuff.c_str());
		if(y > maxY)
			maxY = y;
		patternCells.push_back(sf::Vector2i(std::atoi(xBuff.c_str()), std::atoi(yBuff.c_str())));
	}
	patternFile.close();

	if(maxX >= mapWidth || maxY >= mapHeight)
		return 0;

	map->setLifeCells(patternCells);
	return 1;
}

App::~App(){
	delete map;
	delete gui;
	delete view;
	delete window;
}
