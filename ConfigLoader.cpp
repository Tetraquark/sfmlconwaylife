#include "ConfigLoader.hpp"

Json::Value ConfigLoader::LoadConfig(char* fileName){

	confFile.open(fileName);
	if(!confFile.is_open())
		return NULL;

	std::string buff;

	while(!confFile.eof()){
		confFile >> buff;
		confAsString += buff;
	}

	reader.parse(confAsString, root);

	confFile.close();
	return root;
}
