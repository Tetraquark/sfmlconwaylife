/*
 * 	Class description here.
 *
 *	@author		Tetraquark	| tetraquark.ru
 * 	@change     22.08.2015	Tetraquark
 */

#ifndef CELL_HPP_
#define CELL_HPP_

#include <SFML/Graphics.hpp>

class Cell{

public:
	enum CellState{
		LIFE, DEAD, BIRTH, BORDER
	};

	Cell();
	Cell(float __x, float __y, float __width, float __heigth, sf::Color __color);
	Cell(float __x, float __y, float __width, float __heigth, sf::Color __color, Cell::CellState __state);
	void cellInit(float __x, float __y, float __width, float __heigth, sf::Color __color, Cell::CellState __state);

	void draw(sf::RenderWindow *__window);

	sf::Vector2f getPosition();
	float getX();
	float getY();
	Cell::CellState getState();
	bool getUpdateFlag();

	void setPosition(sf::Vector2f __position);
	void setX(float __x);
	void setY(float __y);
	void setState(Cell::CellState __state, sf::Color __color);
	void setUpdateFlag(bool __flag);

	int posCompare(sf::Vector2i __position);
private:
	float x, y;
	float width, height;
	bool updateFlag;

	sf::Color color;
	sf::Color lastColor;
	sf::RectangleShape cellShape;

	CellState state;
};

#endif /* CELL_HPP_ */
