/*
 * 	Class description here.
 *
 *	@author		Tetraquark	| tetraquark.ru
 * 	@change     22.08.2015	Tetraquark
 */

#ifndef APP_HPP_
#define APP_HPP_

#include <SFML/Graphics.hpp>
#include <fstream>
#include "json/json.h"
#include <iostream>
#include "Uinterface.hpp"
#include "ConfigLoader.hpp"
#include "LifeMap.hpp"

#define MAX_APP_SPEED	150

class App{

private:
	sf::RenderWindow* window;
	sf::View* view;

	LifeMap* map;
	Uinterface* gui;

	char* configFile;
	std::string patternFile;
	int winWidth, winHeight;
	int mapWidth, mapHeight;
	int cellWidth, cellHeight;

	sf::Time FrameTime;
	long int steps;
	bool updatingMap;
	bool createLiveCells;
	bool slideMap;
	int appSpeed;

	ConfigLoader cfLoad;
	Json::Value jsonConf;
	void loadConfigure(char* __configFilePath);
public:
	App();
	App(char* configFilePath);
	~App();
	void initApp(char* __configFilePath);

	void runApp();
	int loadAndSetPattern(const char* __fileName);
};

#endif /* APP_HPP_ */
