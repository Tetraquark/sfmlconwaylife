/*
 * 	Class description here.
 *
 *	@author		Tetraquark	| tetraquark.ru
 * 	@change     22.08.2015	Tetraquark
 */

#ifndef UINTERFACE_HPP_
#define UINTERFACE_HPP_

#include <SFML/Graphics.hpp>
#include <sstream>

#define DEFAULT_FONT 		"data/fonts/BGOTHL.TTF"

#define STR_SPEED 			"Speed: "
#define STR_STEPS 			"Steps: "
#define STR_STATE			"State: "

class Uinterface{

private:
	int charSize;
	sf::Color charColor;
	sf::Font charFont;
	char* fontPath;

	std::string speedStr;
	sf::Text speedText;
	sf::Vector2f speedPos;
	std::string stepsStr;
	sf::Text stepsText;
	sf::Vector2f stepsPos;
	sf::Text gameState;
	sf::Vector2f statePos;

	std::string intToString(float __value);
public:
	Uinterface(int __charSize, sf::Color __charColor);

	int loadFont(char* __fontPath);
	void drawUI(sf::RenderWindow *__window);
	void setSpeed(int __speed);
	void setStepsNum(long int __num);
	void setState(std::string __state);
};

#endif /* UINTERFACE_HPP_ */
